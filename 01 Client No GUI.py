import serial
import pyautogui

ser = serial.Serial("COM10", 115200, timeout=0.2)
pedalCount = 0

while True:
	if ser.in_waiting > 0:
		ser.readline()
		pedalCount += 0.5
		
		if pedalCount >= 3:
			pyautogui.press("up")
			pedalCount = 0
			print("Moving Forward")