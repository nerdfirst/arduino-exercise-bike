#define SENSOR_LEFT 3
#define SENSOR_RIGHT 2

unsigned long left_time;
unsigned long right_time;

void handle_left() {
  unsigned long diff = millis() - left_time;
  if (diff > 400) {
    left_time = millis();
    Serial.println("Left");
  }
}

void handle_right() {
  unsigned long diff = millis() - right_time;
  if (diff > 400) {
    right_time = millis();
    Serial.println("Right");
  }
}

void setup() {
  pinMode(SENSOR_LEFT, INPUT);
  int left_inter_pin = digitalPinToInterrupt(SENSOR_LEFT);
  attachInterrupt(left_inter_pin, handle_left, RISING);
  
  pinMode(SENSOR_RIGHT, INPUT);
  int right_inter_pin = digitalPinToInterrupt(SENSOR_RIGHT);
  attachInterrupt(right_inter_pin, handle_right, RISING);
  
  pinMode(13, OUTPUT);
  Serial.begin(115200);

  left_time = 0;
  right_time = 0;
}

void loop() {
  int left = digitalRead(SENSOR_LEFT);
  int right = digitalRead(SENSOR_RIGHT);

  if (left == HIGH || right == HIGH) {
    digitalWrite(13, HIGH);
  }
  else {
    digitalWrite(13, LOW);
  }
}