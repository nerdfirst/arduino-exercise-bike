#define SENSOR_LEFT 3
#define SENSOR_RIGHT 2
#define DEBOUNCE_TIME 400

int last_left;
int last_right;
unsigned long left_time;
unsigned long right_time;

void setup() {
	pinMode(SENSOR_LEFT, INPUT);
	pinMode(SENSOR_RIGHT, INPUT);
	Serial.begin(115200);
	
	last_left = LOW;
	last_right = LOW;
	left_time = 0;
	right_time = 0;
}

void loop() {
	int left = digitalRead(SENSOR_LEFT);
	int right = digitalRead(SENSOR_RIGHT);
	
	if (left == HIGH && last_left == LOW) {
		unsigned long diff = millis() - left_time;
		if (diff > DEBOUNCE_TIME) {
			Serial.println("Left");
			left_time = millis();
		}
	}
	if (right == HIGH && last_right == LOW) {
		unsigned long diff = millis() - right_time;
		if (diff > DEBOUNCE_TIME) {
			Serial.println("Right");
			right_time = millis();
		}
	}
	
	last_left = left;
	last_right = right;
}