from tkinter import *
import serial
import pyautogui

ser = serial.Serial("COM10", 115200, timeout=0.2)
pedalCount = 0

root = Tk()
root.geometry("400x200")

def mainLogic():
	global pedalCount

	if ser.in_waiting > 0:
		ser.readline()
		pedalCount += 0.5
		totalPedals.set( totalPedals.get()+0.5 )
		
		if pedalCount >= 3:
			pyautogui.press("up")
			pedalCount = 0
			print("Moving Forward")
			
	root.after(1, mainLogic)

totalPedals = DoubleVar()
Label(root, text="Total Count:").pack()
Label(root, textvariable=totalPedals, font=("Tahoma",28), fg="green").pack()

root.after(0, mainLogic)
root.mainloop()