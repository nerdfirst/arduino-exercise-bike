from tkinter import *
from tkinter import ttk
from datetime import datetime as dt

def makeGui(config):
	root = Tk()
	enableValue = IntVar()

	root.title("Cycle Tracker")
	root.configure(background="white")
	root.geometry("720x600")
	root.grid_columnconfigure(0, weight=1, minsize=720/2)
	root.grid_columnconfigure(1, weight=1, minsize=720/2)

	# Top bar
	topLabel = Label(root, text="Pedal to Start!", background="orange", font=("Tahoma", 20), fg="white")
	topLabel.grid(sticky=W+E, row=0, column=0, columnspan=2, padx=5)

	# Top bar progress
	timeDecay = DoubleVar()
	ttk.Progressbar(root, variable=timeDecay, maximum=config["RUNNING_TIMEOUT"]).grid(column=0, row=1, sticky=W+E, padx=5, pady=5, columnspan=2)

	# Count
	countValue = StringVar()
	Label(root, text="Count", background="white", font=("Tahoma", 11)).grid(sticky=W, padx=5, pady=5, column=0, row=2)
	Label(root, textvariable=countValue, background="white", font=("Tahoma Bold", 28), fg="green").grid(column=0, row=3)

	# Time
	timeValue = StringVar()
	Label(root, text="Time", background="white", font=("Tahoma", 11)).grid(sticky=W, padx=5, pady=5, column=1, row=2)
	Label(root, textvariable=timeValue, background="white", font=("Tahoma Bold", 28), fg="blue").grid(column=1, row=3)

	# Calories
	caloriesValue = StringVar()
	Label(root, text="Calories", background="white", font=("Tahoma", 11)).grid(sticky=W, padx=5, pady=5, column=0, row=4)
	Label(root, textvariable=caloriesValue, background="white", font=("Tahoma Bold", 28), fg="red").grid(column=0, row=5)

	# Weight
	weightValue = StringVar()
	Label(root, text="Weight Lost", background="white", font=("Tahoma", 11)).grid(sticky=W, padx=5, pady=5, column=1, row=4)
	Label(root, textvariable=weightValue, background="white", font=("Tahoma Bold", 28), fg="red").grid(column=1, row=5)

	# Speed
	speedValue = StringVar()
	Label(root, text="Speed", background="white", font=("Tahoma", 11)).grid(sticky=W, padx=5, pady=5, column=0, row=6)
	Label(root, textvariable=speedValue, background="white", font=("Tahoma Bold", 28), fg="orange").grid(column=0, row=7)

	# RPM
	rpmValue = StringVar()
	Label(root, text="Cadence", background="white", font=("Tahoma", 11)).grid(sticky=W, padx=5, pady=5, column=1, row=6)
	Label(root, textvariable=rpmValue, background="white", font=("Tahoma Bold", 28), fg="orange").grid(column=1, row=7)

	# Intensity bar
	intensityVar = DoubleVar()
	Label(root, text="Intensity / Burn Rate", background="white", font=("Tahoma", 11)).grid(sticky=W, padx=5, pady=5, column=0, row=8)
	ttk.Progressbar(root, variable=intensityVar, maximum=10).grid(column=0, row=9, sticky=W+E, padx=5, pady=5, columnspan=2)

	# Distance
	distValue = StringVar()
	Label(root, text="Distance", background="white", font=("Tahoma", 11)).grid(sticky=W, padx=5, pady=5, column=0, row=10)
	Label(root, textvariable=distValue, background="white", font=("Tahoma Bold", 28), fg="purple").grid(column=0, row=11)

	needsReset = BooleanVar()
	def resetFunction():
		needsReset.set(True)

	# Street View Controls
	streetViewStatus = DoubleVar()
	Label(root, text="Street View Controls", background="white", font=("Tahoma", 11)).grid(sticky=W, padx=5, pady=5, column=1, row=10)
	Checkbutton(root, text="Enable Control", variable=enableValue, background="white", font=("Tahoma", 11)).grid(column=1, row=11)
	Button(root, text="Reset Progress", command=resetFunction, font=("Tahoma", 11)).grid(column=1, row=12)
	ttk.Progressbar(root, variable=streetViewStatus, maximum=config["GMAPS_SCREEN_DIST"]).grid(column=1, row=13, sticky=W+E, padx=5, pady=5, columnspan=2)
	
	linkedVariables = {
		"topLabel": topLabel,
		"enableValue": enableValue,
		"timeDecay": timeDecay,
		"countValue": countValue,
		"timeValue": timeValue,
		"caloriesValue": caloriesValue,
		"weightValue": weightValue,
		"speedValue": speedValue,
		"rpmValue": rpmValue,
		"intensityVar": intensityVar,
		"distValue": distValue,
		"needsReset": needsReset,
		"streetViewStatus": streetViewStatus
	}
	
	return (root, linkedVariables)