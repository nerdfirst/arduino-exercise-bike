config = {
	"METRE_DEVELOPMENT": 4.2,
	"RUNNING_TIMEOUT": 0.75,
	"GMAPS_SCREEN_DIST": 10.2
}

from tkinter import *
from tkinter import ttk
from datetime import datetime as dt

import pyautogui
import serial
import _thread
import time
import json

import bike_gui
import bike_logic

root, guiVars = bike_gui.makeGui(config)

_thread.start_new_thread(bike_logic.serialLogic, (guiVars,config))
root.mainloop()