from tkinter import *
from tkinter import ttk
from datetime import datetime as dt

import pyautogui
import serial
import _thread
import time
import json

def serialLogic(gui, config):
	ser = serial.Serial("COM10", 115200, timeout=0.2)
	
	isMoving = False
	pedalLastSeen = dt.now()
	loopLastTime = dt.now()
	
	firstPedal = None
	
	totalTime = 0
	
	totalCount = 0
	caloriesBurned = 0
	distance = 0
	rpm = 0
	kph = 0
	distanceForGMaps = 0
	
	totalCount,totalTime,caloriesBurned,distance  = loadState()		
	perPedalRedraw(gui, totalCount, caloriesBurned, distance, 0, 0, distanceForGMaps)
		
	while True:
		currentTime = dt.now()
		
		pedalTimeObj = currentTime - pedalLastSeen
		pedalTime =  pedalTimeObj.seconds + pedalTimeObj.microseconds/1000000
		if pedalTime > config["RUNNING_TIMEOUT"]:
			isMoving = False
			firstPedal = None
			gui["topLabel"].configure(text="Stopped", background="red")
			perPedalRedraw(gui, totalCount, caloriesBurned, distance, 0, 0, distanceForGMaps)
			
		gui["timeDecay"].set(config["RUNNING_TIMEOUT"] - pedalTime if pedalTime >= 0 else 0)
	
		if ser.in_waiting > 0:
			value = ser.readline().decode("UTF-8","ignore").strip()
			
			if value == "Left" or value == "Right":
				if firstPedal == "Left" and value == "Right":
					totalCount += 1
				elif firstPedal == "Right" and value == "Left":
					totalCount += 1
			
				gui["topLabel"].configure(text="Moving", background="green")
				
				if pedalLastSeen == -1 or isMoving == False:
					firstPedal = value
					pedalLastSeen = currentTime
					isMoving = True
				else:
					diff = currentTime - pedalLastSeen
					interval = (diff.seconds + (diff.microseconds/1000000)) * 2.0
					pedalLastSeen = currentTime
					
					rpm = 60/interval
					
					distance += config["METRE_DEVELOPMENT"] / 2.0
					distanceForGMaps += config["METRE_DEVELOPMENT"] / 2.0
					
					mps = 4.4 / interval
					kph = (mps/1000)*3600
					burnRate = calcBurnRate(kph)

					caloriesBurned += burnRate * interval / 2.0
			
			if distanceForGMaps >= config["GMAPS_SCREEN_DIST"]:
				if gui["enableValue"].get() != 0:
					pyautogui.press("up")
				distanceForGMaps -= config["GMAPS_SCREEN_DIST"]
				
			perPedalRedraw(gui, totalCount, caloriesBurned, distance, rpm, kph, distanceForGMaps)
				
		with open("saved_state.json", "w") as f:
			data = json.dump({"totalCount":totalCount, "totalTime":totalTime, "caloriesBurned":caloriesBurned, "distance":distance}, f)
		
		loopTimeDiffObj = (currentTime - loopLastTime)
		loopTimeDiff = loopTimeDiffObj.seconds + loopTimeDiffObj.microseconds/1000000
		
		if isMoving:
			totalTime += loopTimeDiff
			
		# Show Timer
		seconds = totalTime % 60
		minutes = int((totalTime // 60) % 60)
		hours = int((totalTime // 3600))
		gui["timeValue"].set("{:02}:{:02}:{:02.2f}".format(hours,minutes,seconds))
		
		
		if gui["needsReset"].get() == True:
			totalCount = 0
			totalTime = 0
			caloriesBurned = 0
			distance = 0
			with open("saved_state.json", "w") as f:
				data = json.dump({"totalCount":totalCount, "totalTime":totalTime, "caloriesBurned":caloriesBurned, "distance":distance}, f)
			gui["needsReset"].set(False)
		
		loopLastTime = currentTime
		time.sleep(0.016)
		
def perPedalRedraw(gui, totalCount, caloriesBurned, distance, rpm, kph, distanceForGMaps):
		gui["countValue"].set(totalCount)
		gui["caloriesValue"].set("{:.1f} cal".format(caloriesBurned))
		gui["rpmValue"].set("{:.1f} RPM".format(rpm))
		gui["speedValue"].set("{:.1f} km/h".format(kph))
		gui["streetViewStatus"].set(distanceForGMaps)
		showWeightLost(gui, caloriesBurned)
		showDistance(gui, distance)
		showIntensityBar(gui, kph)
			
		
def showWeightLost(gui, caloriesBurned):
	weightLost = caloriesBurned * 0.06479885714
	if weightLost < 10:
		gui["weightValue"].set("{:.3f} g".format(weightLost))
	elif weightLost < 100:
		gui["weightValue"].set("{:.2f} g".format(weightLost))
	elif weightLost < 1000:
		gui["weightValue"].set("{:.1f} g".format(weightLost))
	else:
		gui["weightValue"].set("{:.3f} kg".format(weightLost/1000))
		
def showDistance(gui, distance):
	if distance < 1000:
		gui["distValue"].set("{:.1f} m".format(distance))
	elif distance < 10000:
		gui["distValue"].set("{:.2f} km".format(distance/1000))
	else:
		gui["distValue"].set("{:.1f} km".format(distance/1000))
	
def showIntensityBar(gui, kph):
	mph = kph * 0.621371
	if mph == 0:
		gui["intensityVar"].set(0)
	elif mph <= 10:
		gui["intensityVar"].set(0.1)
	else:
		gui["intensityVar"].set(mph-10)
		
def calcBurnRate(kph):
	mph = kph * 0.621371
	burnRate = 0
	if mph < 10:
		burnRate = 0.078055556
	elif mph < 12:
		burnRate = 0.117222222
	elif mph < 14:
		burnRate = 0.156388889
	elif mph < 16:
		burnRate = 0.195555556
	elif mph < 19:
		burnRate = 0.234444444
	else:
		burnRate = 0.312777778
		
	return burnRate
	
def loadState():
	totalCount,totalTime,caloriesBurned,distance = 0,0,0,0

	try:
		with open("saved_state.json", "r") as f:
			data = json.load(f)
			totalCount = data["totalCount"]
			totalTime = data["totalTime"]
			caloriesBurned = data["caloriesBurned"]
			distance = data["distance"]
	except:
		with open("saved_state.json", "w") as f:
			data = json.dump({"totalCount":totalCount, "totalTime":totalTime, "caloriesBurned":caloriesBurned, "distance":distance}, f)
			
	return (totalCount,totalTime,caloriesBurned,distance)