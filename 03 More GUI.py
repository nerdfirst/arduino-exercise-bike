from tkinter import *
from datetime import datetime as dt
import serial
import pyautogui

ser = serial.Serial("COM10", 115200, timeout=0.2)
pedalCount = 0
lastTiming = dt.now()

root = Tk()
root.geometry("400x200")

def mainLogic():
	global pedalCount
	global lastTiming

	if ser.in_waiting > 0:
		ser.readline()
		pedalCount += 0.5
		totalPedals.set( totalPedals.get()+0.5 )
		
		if pedalCount >= 3:
			pyautogui.press("up")
			pedalCount = 0
			print("Moving Forward")
			
		diff = dt.now() - lastTiming
		interval = (diff.seconds + (diff.microseconds/1000000))
		if interval > 1:
			lastTiming = dt.now()
		else:
			oneCompletePedalTime = interval * 2.0
			rpm = 60.0 / oneCompletePedalTime
			rpmText.set( "{:.1f} RPM".format(rpm) )
			lastTiming = dt.now()
		
	root.after(1, mainLogic)

	
totalPedals = DoubleVar()
Label(root, text="Total Count:").pack()
Label(root, textvariable=totalPedals, font=("Tahoma",28), fg="green").pack()

rpmText = StringVar()
Label(root, text="Cadence:").pack()
Label(root, textvariable=rpmText, font=("Tahoma",28), fg="blue").pack()

root.after(0, mainLogic)
root.mainloop()