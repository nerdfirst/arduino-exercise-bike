#define SENSOR_LEFT 3
#define SENSOR_RIGHT 2

void setup() {
	pinMode(SENSOR_LEFT, INPUT);
	pinMode(SENSOR_RIGHT, INPUT);
	Serial.begin(115200);
}

void loop() {
	if (digitalRead(SENSOR_LEFT) == HIGH) {
		Serial.println("Left");
	}
	else if (digitalRead(SENSOR_RIGHT) == HIGH) {
		Serial.println("Right");
	}
	else {
		Serial.println("None");
	}
}