# About this Code

The code in this repository is part of a tutorial by **0612 TV** entitled **Programming an Exercise Bike**, hosted on YouTube.

[![Click to Watch](https://img.youtube.com/vi/emS56uT_-zQ/0.jpg)](https://www.youtube.com/watch?v=emS56uT_-zQ "Click to Watch")


# Installation & Use

## Arduino Code

For organizational simplicity, the Arduino code files have not been packaged as proper projects. The easiest way to use them is to open them in a text editor and copy-paste their contents into the Arduino IDE. Alternatively, create a folder with the same name as the .ino file and place the file inside.

## Python Code

The Python code in this repository relies on several libraries. To install them, please run each of the following commands in the command line.

`pip install pyserial`

`pip install pyautogui`

`pip install tkinter` (May be optional in certain installations)


# License

This project is licensed under the **Apache License 2.0**. For more details, please refer to LICENSE.txt within this repository.