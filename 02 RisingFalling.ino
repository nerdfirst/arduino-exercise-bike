#define SENSOR_LEFT 3
#define SENSOR_RIGHT 2

int last_left;
int last_right;

void setup() {
	pinMode(SENSOR_LEFT, INPUT);
	pinMode(SENSOR_RIGHT, INPUT);
	Serial.begin(115200);
	
	last_left = LOW;
	last_right = LOW;
}

void loop() {
	int left = digitalRead(SENSOR_LEFT);
	int right = digitalRead(SENSOR_RIGHT);
	
	if (left == HIGH && last_left == LOW) {
		Serial.println("Left");
	}
	if (right == HIGH && last_right == LOW) {
		Serial.println("Right");
	}
	
	last_left = left;
	last_right = right;
}